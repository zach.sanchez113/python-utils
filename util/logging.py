"""Utility function for setting up logging.

TODO: Add a way to add a log filtering function.
"""

from __future__ import annotations

import logging
import os
import re
import sys
from collections.abc import Callable
from textwrap import dedent
from typing import TYPE_CHECKING, Any

from loguru import logger
from path import Path
from pyrsistent import PMap, m

from util.common import env_bool, in_jupyter

if TYPE_CHECKING:
    from loguru import Record


# Default log message format (multiline to make it more readable)
LOG_FORMAT_DEFAULT = """
    <green>{time:YYYY-MM-DD HH:mm:ss}</green> | <level>{level: <8}</level>
    | <cyan>{name}</cyan>:<cyan>{function}</cyan>:<cyan>{line}</cyan> - <level>{message}</level>
"""

LOG_FORMAT = re.sub(r"\n", "", dedent(LOG_FORMAT_DEFAULT))

LOGURU_DEFAULT_KWARGS: PMap[str, Any] = m(
    format=LOG_FORMAT,
    colorize=None,
    backtrace=False,
    diagnose=False,
    enqueue=True,
    catch=True,
)

LOGURU_FILE_KWARGS: PMap[str, Any] = m(
    rotation="50MB",
    retention=5,
    compression="gz",
)


class InterceptHandler(logging.Handler):
    """Redirect all stdlib logging to loguru."""

    def emit(self, record: logging.LogRecord):
        # Get corresponding Loguru level if it exists
        try:
            level = logger.level(record.levelname).name
        except ValueError:
            level = record.levelno

        # Find caller from where originated the logged message
        frame, depth = sys._getframe(6), 6  # pyright: ignore[reportPrivateUsage]
        while frame and frame.f_code.co_filename == logging.__file__:
            frame = frame.f_back
            depth += 1

        logger.opt(depth=depth, exception=record.exc_info).log(level, record.getMessage())


def get_log_level() -> str:
    """Get the current log level.

    Returns:
        str: Log level.
    """

    if os.environ.get("LOGURU_LEVEL"):
        return os.environ["LOGURU_LEVEL"].upper()
    else:
        return "INFO"


def configure_logging(
    log_level: str = "",
    log_file: Path | None = None,
    log_filter: Callable[[Record], bool] | None = None,
    intercept_stdlib_logging: bool = False,
) -> None:
    """Configure logging with loguru.

    In order, log level/file is determined by:

    1. Function argument
    2. Environment variable
    3. Configuration file (you need to add this one yourself!)

    If log level isn't found, a default of `INFO` is used.

    If log file isn't found, log messages will only be printed to stdout.

    NOTE: Adjust this to your codebase as needed. This is pure boilerplate.

    Args:
        log_level (str, optional): Log level. Defaults to INFO.
        log_file (Path | None, optional): Log file to use if desired. Defaults to None.
        log_filter (Callable[[Record], bool] | None, optional): A directive optionally used to decide for each
            logged message whether it should be sent to the sink or not. This is particularly useful when
            intercepting standard library logging. Defaults to None.
        intercept_stdlib_logging (bool, optional): Redirect standard library logging to loguru. Defaults to False.
    """

    loguru_kwargs = LOGURU_DEFAULT_KWARGS

    if log_level:
        loguru_kwargs = loguru_kwargs.set("level", log_level)
    # Check if environment variables contain log level if not specifed
    else:
        if os.environ.get("LOGURU_LEVEL"):
            loguru_kwargs = loguru_kwargs.set("level", os.environ["LOGURU_LEVEL"].upper())
        else:
            loguru_kwargs = loguru_kwargs.set("level", "INFO")

    # Check if environment variables contain a log file if not specifed
    if not log_file:
        if os.environ.get("LOGURU_LOG_FILE"):
            log_file = Path(os.environ["LOGURU_LOG_FILE"])

    # Local variables may be desired in non-production environments
    loguru_kwargs = loguru_kwargs.set("diagnose", env_bool("LOGURU_DIAGNOSE", default=False))

    # Passing log messages through a multiprocessing-safe queue is generally desireable, but not
    # always, e.g. when running under pytest
    loguru_kwargs = loguru_kwargs.set("enqueue", env_bool("LOGURU_ENQUEUE", default=True))

    # Allow a filtering function to be used if desired so noisy messages are excluded
    loguru_kwargs = loguru_kwargs.set("filter", log_filter)

    # Forcibly enable color and deinitialize colorama if in Jupyter since it's not a TTY, but colors
    # are still nice in that case
    #
    # Also set enqueue to False since it can mess with the output
    if in_jupyter():
        try:
            import colorama  # pyright: ignore

            colorama.deinit()
            loguru_kwargs = loguru_kwargs.set("colorize", True).set("enqueue", False)

        except ImportError:
            ...

    handlers: list[dict[str, Any]] = [
        dict(sink=sys.stdout, **loguru_kwargs),
    ]

    if log_file:
        handlers.append(dict(sink=log_file.expand().abspath(), **loguru_kwargs, **LOGURU_FILE_KWARGS))

    # Clear existing loggers
    logger.remove()

    # Create handlers + custom coloring
    logger.configure(
        handlers=handlers,
        levels=[
            {"name": "INFO", "color": "<blue><bold>"},
            {"name": "DEBUG", "color": "<magenta><bold>"},
        ],
    )

    if intercept_stdlib_logging:
        logging.basicConfig(handlers=[InterceptHandler()], level=0)
