"""Utility functions for `click` error handling and option generation + parsing."""

from __future__ import annotations

import sys
from collections.abc import Sequence
from typing import Any

import rich_click as click
from click.core import Option
from loguru import logger
from path import Path

# Prevent verboseness
DEFAULT_CLICK_FILE = click.Path(
    exists=True,
    file_okay=True,
    dir_okay=False,
    resolve_path=True,
    path_type=Path,
)

DEFAULT_CLICK_FILE_OUTPUT = click.Path(
    exists=False,
    file_okay=True,
    dir_okay=False,
    resolve_path=True,
    path_type=Path,
)

DEFAULT_CLICK_DIR = click.Path(
    exists=True,
    file_okay=False,
    dir_okay=True,
    resolve_path=True,
    path_type=Path,
)


class LoguruCatchGroup(click.RichGroup):
    """Subclass of RichGroup that wraps commands in loguru's `logger.catch` context manager."""

    def __call__(self, *args: Any, **kwargs: Any):
        with logger.catch(onerror=lambda e: sys.exit(1), exclude=(click.ClickException, click.Abort)):
            return super().__call__(*args, **kwargs)


class LoguruCatchCommand(click.RichCommand):
    """Subclass of RichCommand that wraps commands in loguru's `logger.catch` context manager."""

    def __call__(self, *args: Any, **kwargs: Any):
        with logger.catch(onerror=lambda e: sys.exit(1), exclude=(click.ClickException, click.Abort)):
            return super().__call__(*args, **kwargs)


def split_click_option(
    ctx: click.Context,
    param: Option,
    arg: str | Sequence[str] | None,
) -> list[str | int] | None:
    """Turn comma-delimited string into a list and validate each element if possible.

    Args:
        ctx (click.Context): Click context object.
        param (click.core.Option): Parameter passed in as part of Click's validation.
        arg (str | Sequence[str] | None): What was passed in.

    Returns:
        list[str | int] | None: Provided value(s) as a list.
    """

    if arg is None:
        return arg

    if isinstance(arg, str):
        args = [int(i) if i.isdigit() else i for i in arg.split(",")]
    else:
        args = []
        for a in arg:
            args.extend(a.split(","))

    return list(set(args))
