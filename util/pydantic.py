"""Base models for Pydantic."""

from __future__ import annotations

from pydantic import BaseModel as PydanticBaseModel
from pydantic import ConfigDict

model_config_immutable: ConfigDict = ConfigDict(
    # Whether arbitrary types are allowed for field types.
    arbitrary_types_allowed=True,
    # Whether to ignore, allow, or forbid extra attributes during model initialization.
    extra="forbid",
    # Whether models are faux-immutable.
    frozen=True,
    # Whether an aliased field may be populated by its name as given by the model attribute, as well as
    # the alias.
    populate_by_name=True,
    # By default, Pydantic will attempt to coerce values to the desired type when possible. Instead of
    # coercing data, raise an error.
    strict=True,
    # Whether to validate the data when the model is changed.
    validate_assignment=True,
    # Whether to validate default values during validation.
    validate_default=True,
    # Whether to validate the return value from call validators.
    validate_return=True,
)

model_config_mutable: ConfigDict = ConfigDict(
    # Whether arbitrary types are allowed for field types.
    arbitrary_types_allowed=True,
    # Whether to ignore, allow, or forbid extra attributes during model initialization.
    extra="forbid",
    # Whether models are faux-immutable.
    frozen=False,
    # Whether an aliased field may be populated by its name as given by the model attribute, as well as
    # the alias.
    populate_by_name=True,
    # By default, Pydantic will attempt to coerce values to the desired type when possible. Instead of
    # coercing data, raise an error.
    strict=True,
    # Whether to validate the data when the model is changed.
    validate_assignment=True,
    # Whether to validate default values during validation.
    validate_default=True,
    # Whether to validate the return value from call validators.
    validate_return=True,
)


class BaseModelReadOnly(PydanticBaseModel):
    """Read-only Pydantic model with typical config."""

    model_config = model_config_immutable


class BaseModelMutable(PydanticBaseModel):
    """Mutable Pydantic model with typical config."""

    model_config = model_config_mutable
