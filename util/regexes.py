from __future__ import annotations

import re

IP_MATCH = re.compile(r"^[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3}$")
CIDR_MATCH = re.compile(r"^[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3}/[\d]{1,2}$")
ASN_MATCH = re.compile(r"^AS[\d]+$")
USERNAME_MATCH = re.compile(r"^([a-z]+[\w\-]*[a-z0-9]+)$", flags=re.IGNORECASE)
