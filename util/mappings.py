"""Classes that override `dict`.

TODO: Actually use/test this.

TODO: Implement the `Mapping` protocol?
"""

from __future__ import annotations

from typing import Any


class OverrideDict(dict[Any, Any]):
    """Template for overriding 'dict'"""

    def __init__(self, *args: Any, **kwargs: Any):
        super().__init__(*args, **kwargs)

    def __setitem__(self, __k: Any, __v: Any):
        return super().__setitem__(__k, __v)

    def __getitem__(self, __k: Any):
        return super().__getitem__(__k)

    def __delitem__(self, __k: Any):
        return super().__delitem__(__k)

    def __contains__(self, __k: Any):
        return super().__contains__(__k)

    def __or__(self, __o: Any):
        return super().__or__(__o)

    def __ior__(self, __o: Any):
        return super().__ior__(__o)

    def copy(self):
        return super().copy()

    def clear(self):
        return super().clear()

    def get(self, *args: Any):
        # __key, __default = args[0], args[1]
        return super().get(*args)

    def fromkeys(self, *args: Any):
        # __iterable, __value = args[0], args[1]
        return super().fromkeys(*args)

    def items(self):
        return super().items()

    def keys(self):
        return super().keys()

    def pop(self, *args: Any):
        # __key, __default = args[0], args[1]
        return super().pop(*args)

    def popitem(self):
        return super().popitem()

    def setdefault(self, *args: Any):
        # __key, __default = args[0], args[1]
        return super().setdefault(*args)

    def update(self, __m: dict[Any, Any] | None, **kwargs: Any):
        if __m:
            return super().update(__m, **kwargs)
        else:
            return super().update(**kwargs)

    def values(self):
        return super().values()


class CaseInsensitiveDict(dict[Any, Any]):
    """Dictionary with case-insensitive lookups"""

    def __init__(self, *args: Any, **kwargs: Any):
        super().__init__(*args, **kwargs)
        self._lowercase_map = {self._to_lowercase(__k): __k for __k in super().keys()}

    def _to_lowercase(self, __k: Any):
        try:
            return __k.lower()
        except Exception:
            return __k

    def _to_normal_case(self, __k: Any):
        for lower_key, normal_key in super().items():
            if lower_key == self._to_lowercase(__k):
                return normal_key
        return __k

    def __setitem__(self, __k: Any, __v: Any):
        self._lowercase_map[self._to_lowercase(__k)] = __k
        return super().__setitem__(self._to_normal_case(__k), __v)

    def __getitem__(self, __k: Any):
        return super().__getitem__(self._to_lowercase(__k))

    def __delitem__(self, __k: Any):
        normal_key = self._to_normal_case(__k)
        del self._lowercase_map[self._to_lowercase(__k)]
        return super().__delitem__(normal_key)

    def __contains__(self, __k: Any):
        return super().__contains__(self._to_normal_case(__k))

    # TODO
    def __or__(self, __o: Any):
        return super().__or__(__o)

    # TODO
    def __ior__(self, __o: Any):
        return super().__ior__(__o)

    # TODO: This isn't awesome
    def copy(self):
        return CaseInsensitiveDict(super().copy())

    def clear(self):
        self._lowercase_map.clear()
        return super().clear()

    # TODO
    def get(self, *args: Any):
        # __key, __default = args[0], args[1]
        return super().get(*args)

    # TODO
    def fromkeys(self, *args: Any):
        # __iterable, __value = args[0], args[1]
        return super().fromkeys(*args)

    # TODO
    def pop(self, *args: Any):
        # __key, __default = args[0], args[1]
        return super().pop(*args)

    # TODO
    def setdefault(self, *args: Any):
        # __key, __default = args[0], args[1]
        return super().setdefault(*args)

    # TODO
    def update(self, __m: dict[Any, Any] | None, **kwargs: Any):
        if __m:
            return super().update(__m, **kwargs)
        else:
            return super().update(**kwargs)
