"""The most common utility functions that I like to have available in pretty much every project."""

from __future__ import annotations

import datetime
import os
import shutil
import subprocess
import sys
import unicodedata
from collections import OrderedDict
from collections.abc import Generator, Mapping, Sequence
from typing import Any, TypeVar

import dynaconf.utils.functional
import requests
from loguru import logger
from path import Path
from requests.adapters import HTTPAdapter, Retry

# Generics
T = TypeVar("T")


class Unreachable(Exception):  # noqa: N818
    """Indicates unreachable code.

    This is useful any time that the type checker can't determine that some code is unreachable.
    Of course, this also serves as a nice safeguard at runtime if a logical error is introduced by mistake.

    NOTE: In Python >= 3.11, `typing.assert_never` should be used instead.
    """


class T_NOT_SET:  # noqa: N801
    def __repr__(self) -> str:
        return f"{__name__}.{self.__class__.__name__}"

    def __copy__(self) -> Any:
        return NOT_SET

    def __deepcopy__(self, memodict: dict[Any, Any] | None = None) -> Any:
        return NOT_SET


NOT_SET = T_NOT_SET()


def enumerate_with_status(
    iterable: Sequence[T],
    interval: int = 0,
) -> Generator[tuple[int, T], None, None]:
    num_elements = len(iterable)
    pad = len(str(num_elements))
    for i, obj in enumerate(iterable):
        if interval and i % interval == 0:
            print(f"On {i:>{pad}} / {num_elements:>{pad}}")  # noqa: T201
        yield i, obj


def clean_multiline(s: str, delimiter: str = "\n\n"):
    result = ""
    for part in s.strip().split("\n"):
        if part.strip():
            result += part.strip() + " "
        else:
            result += delimiter

    return result


def env_bool(key: str, default: bool = False) -> bool:
    """Check if an environment variable is set to a truth-like value.

    Specifically, if the value is in ["1", "true", "yes"], then it's considered to be true. Anything
    else is assumed to be false.

    Args:
        key (str): Environment variable name.
        default (bool, optional): Fallback if the environment variable isn't found. Defaults to False.

    Returns:
        bool: Whether the key is set to a truth-like value.
    """

    if (value := os.environ.get(key)) is None:
        return default

    return value.lower() in ["1", "true", "yes"]


def type_name(value: Any) -> str:
    """Return the name of the given value's type."""

    if hasattr(type(value), "__module__") and type(value).__module__ and type(value).__module__ != "builtins":
        return f"{type(value).__module__}.{type(value).__name__}"
    else:
        return type(value).__name__


def walk_nested(
    obj: Sequence[Any] | Mapping[Any, Any] | dynaconf.utils.functional.LazyObject,
    *args: Any,
    default: Any = None,
    all_or_nothing: bool = True,
) -> Any:
    """Walk the nested objects inside 'obj' with the specified *args as key names and return the value at the end.

    This is slightly more flexible than obj.get('foo', {}).get('bar', {}).get('baz') because a TypeError can be thrown
    if the key name exists but is not a dictionary.

    Examples:
        obj = {'foo': {'bar': {'baz': 1}}, 'buzz': 'ERROR' }
        walk_nested(obj, 'foo', 'bar', 'baz')  # 1
        walk_nested(obj, 'foo', 'bar', 'buzz', all_or_nothing=False)  # {'baz': 1}
        walk_nested(obj, 'buzz', 'baz', all_or_nothing=False) # 'ERROR'

    Args:
        obj (Sequence | Mapping): A dictionary/iterable-like object containing nested dictionaries/elements.
        *args (Any): The individual keys and/or indices to walk.
        default (Any, optional): Default value to use if an exception occurs. Defaults to None.
        all_or_nothing (bool, optional): If True and an error is encountered, return the default. If False, return the most recent obj. Defaults to True.

    Returns:
        Any: The return value, determined per above.
    """

    try:
        for arg in args:
            obj = obj[arg]
    except (KeyError, TypeError, IndexError, AttributeError):
        return default if all_or_nothing else obj
    else:
        return obj


def convert_json(obj: Any) -> Any:
    """A sane function for json.dump(s)' `default` to convert Python data types to JSON-compatible ones."""

    if isinstance(obj, set | tuple):
        return list(obj)
    if isinstance(obj, OrderedDict):
        return dict(obj)
    if isinstance(obj, datetime.datetime):
        return obj.replace(microsecond=0).isoformat()
    if isinstance(obj, bytes):
        try:
            return obj.decode(encoding="utf-8")
        except UnicodeDecodeError:
            ...

    try:
        from django.db.models import Model, QuerySet  # pyright: ignore
        from django.forms.models import model_to_dict  # pyright: ignore

        if isinstance(obj, QuerySet):
            if isinstance(obj[0], Model):
                return [model_to_dict(o) for o in obj]
            else:
                return list(obj)

        if isinstance(obj, Model):
            return model_to_dict(obj)

    except ImportError:
        pass

    try:
        return str(obj)
    except Exception:
        return repr(obj)


def retry_request(
    method: str,
    url: str,
    max_retries: int = 3,
    status_forcelist: Sequence[int] = (429, 500, 502, 503, 504),
    backoff_factor: int = 0,
    **kwargs: Any,
) -> requests.models.Response:
    """Retry an API call a set number of times.

    This will retry a request 'max_retries' times, and will return the response object once
    'max_retries' is hit or the request succeeds.

    Args:
        method (str):  HTTP method of request.
        url (str): Endpoint URL including protocol.
        max_retries (int, optional): Max number of retries before quitting. Defaults to 3.
        status_forcelist (Sequence[int], optional): A set of integer HTTP status codes that we should force a retry on.
        backoff_factor (float, optional): A backoff factor to apply between attempts after the second try. Defaults to 0 (disabled).
        **kwargs: Arbitrary kwargs for requests.request.

    Returns:
        requests.models.Response: Response from API call.
    """

    session = requests.Session()
    retry = Retry(total=max_retries, backoff_factor=backoff_factor, status_forcelist=status_forcelist)
    protocol = url.split("://", 1)[0] + "://"
    session.mount(protocol, HTTPAdapter(max_retries=retry))
    return session.request(method, url, **kwargs)


def pgp_decrypt(input_file: Path, password_file: Path, output_file: Path) -> None:
    """Decrypt a PGP-encrypted file.

    WARNING: This currently only supports Linux.

    Args:
        input_file (Path): Input file.
        password_file (Path): Password file for decryption.
        output_file (Path): Where the decrypted file should go.

    Raises:
        Exception: If this function is run on a non-Unix machine.
        Exception: If any of the input files don't exist or aren't readable.
        Exception: If PGP decryption fails.
    """

    input_file = input_file.expand()
    password_file = password_file.expand()
    output_file = output_file.expand()

    # Basic sanity checks
    if not is_unix():
        raise Exception("This function only works on Linux.")
    if not input_file.exists():
        raise Exception(f"Input file {input_file} doesn't exist!")
    if not input_file.access(os.R_OK):
        raise Exception(f"Input file {input_file} isn't readable!")
    if not password_file.exists():
        raise Exception(f"Password file {password_file} doesn't exist!")
    if not password_file.access(os.R_OK):
        raise Exception(f"Password file {password_file} isn't readable!")

    # Locate gpg executable
    if not (gpg_command_path := shutil.which("gpg")):
        raise Exception("Could not locate GPG command!")

    # Do the thing
    command = f"{gpg_command_path} --decrypt --pinentry-mode loopback --batch --passphrase-file {password_file} {input_file} > {output_file}"
    result = subprocess.run(command, shell=True)

    logger.debug(f"{command = }")
    logger.debug(f"{result.returncode = }")
    logger.debug(f"{result.stdout.decode() = }")
    logger.debug(f"{result.stderr.decode() = }")

    if result.returncode != 0:
        raise Exception(f"Failed to PGP-decrypt {input_file}: {result.stderr.decode()}")


def remove_unicode_control_chars(s: str) -> str:
    """Remove all unicode control characters from a given string.

    Args:
        s (str): String to remove unicode from.

    Returns:
        str: Mutated string.
    """

    return "".join(ch for ch in s if unicodedata.category(ch)[0] != "C")


def is_unix(allow_cygwin: bool = True) -> bool:
    """Determine whether the host is Unix(-like).

    Args:
        allow_cygwin (bool, optional): Whether running in Cygwin should count. Defaults to True.

    Returns:
        bool: Whether this system is Unix(-like).
    """

    # The last two are WASM
    forbidden_platforms = ["win32", "emscripten", "wasi"]
    if not allow_cygwin:
        forbidden_platforms.append("cygwin")
    return sys.platform not in forbidden_platforms


def in_jupyter() -> bool:
    """Are we running in a Jupyter notebook?

    Credit: https://stackoverflow.com/a/39662359/11832705

    Returns:
        bool: Whether the code is being run in a Jupyter notebook.
    """

    try:
        shell = get_ipython().__class__.__name__  # pyright: ignore[reportUndefinedVariable]  # noqa: F821
        if shell == "ZMQInteractiveShell":
            return True
    except Exception:
        ...

    return False
