# python-utils

Various Python utility functions and classes that are useful for bootstraping a project.

Of course, this can also be used as a template on its own, just be sure to adjust the config in
`pyproject.toml` and `.pre-commit-config.yaml` if using a different Python version.
